﻿using System;
using Sitecore;
using Sitecore.Shell.Applications.ContentEditor;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Web.UI.Sheer;
using System.Linq;

namespace MyApp.SitecoreCustom.GMapField
{
    public class GoogleMapField : Sitecore.Shell.Applications.ContentEditor.Text, IContentField
    {
        private System.Web.UI.WebControls.Image _mapImageCtrl;
        private int _mapWidth = 600;
        private int _mapHeight = 150;
        private int _mapZoomFactor = 14;

        #region Implementation of IContentField

        public string GetValue()
        {
            return Value;
        }

        public void SetValue(string value)
        {
            Value = value;
        }

        #endregion

        #region Properties

        //public string Source
        //{
        //    get
        //    {
        //        return GetViewStateString("Source");
        //    }
        //    set
        //    {
        //        //Assert.ArgumentNotNullOrEmpty(value, "value");
        //        SetViewStateString("Source", value);
        //    }
        //}

        #endregion

        #region Override

        protected override void Render(System.Web.UI.HtmlTextWriter output)
        {
            base.Render(output);

            //render other control
            var value = GetValue();

            if(!string.IsNullOrEmpty(value)){
                //get lat lng
                var position = value.Split(',');

                if(position.Count() == 2)
                {
                    double lat = 0;
                    double lng = 0;

                    double.TryParse(position[0], out lat);
                    double.TryParse(position[1], out lng);

                    _mapImageCtrl = new System.Web.UI.WebControls.Image();
                    _mapImageCtrl.ID = ID + "_Img_MapView";
                    _mapImageCtrl.CssClass = "imageMapView";
                    _mapImageCtrl.Width = _mapWidth;
                    _mapImageCtrl.Height = _mapHeight;
                    _mapImageCtrl.ImageUrl = GetGoogleMapImageUrl();
                    _mapImageCtrl.Style.Add("padding-top", "5px");

                    _mapImageCtrl.RenderControl(output);
                }
            }
        }

        public override void HandleMessage(Sitecore.Web.UI.Sheer.Message message)
        {
            //let base class do what it's need to do.
            base.HandleMessage(message);

            //And after that, we will take a charge from here :P
            if (message["id"] != ID || string.IsNullOrWhiteSpace(message.Name)) return;

            switch (message.Name)
            {
                case "customGmap:SetLocation": //It defined in core database
                    Sitecore.Context.ClientPage.Start(this, "SetLocation");
                    return;
                case "customGmap:ClearLocation": //it also defined in core database
                    Sitecore.Context.ClientPage.Start(this, "ClearLocation");
                    return;
            }

            if (Value.Length > 0) SetModified();

            Value = string.Empty;
        }

        //protected override void SetModified()
        //{
        //    base.SetModified();
        //    if (base.TrackModified)
        //    {
        //        Sitecore.Context.ClientPage.Modified = true;
        //    }
        //}

        #endregion

        #region Message Handler

        protected void SetLocation(ClientPipelineArgs args)
        {
            //Check if popup windows is postback
            if (args.IsPostBack)
            {
                //check whether popup windows has selected value
                if (args.HasResult && Value.Equals(args.Result) == false)
                {
                    //tell content editor that value in field is modified
                    SetModified();

                    //set current field value with selected value from popup window
                    SetValue(args.Result);
                }
            }
            else
            {
                //show popup
                //get popup control that named GmapLocationPickerDialog
                var url = UIUtil.GetUri("control:GmapLocationPickerDialog");

                //Try to get Current Value (if it previously has a value)
                var value = GetValue();
                if (!string.IsNullOrEmpty(value))
                {
                    //passing current value to querystring so that it could read by our popup window
                    url = string.Format("{0}&value={1}",
                        url, value);
                }

                //Show our popup dialog
                SheerResponse.ShowModalDialog(url, "800", "600", "", true);

                //Wait popup dialog for postback
                args.WaitForPostBack();
            }
        }

        protected void ClearLocation(ClientPipelineArgs args)
        {
            //set empty value
            SetValue(string.Empty);

            //tell content editor that value in field is modified
            SetModified();
        }
        #endregion

        #region Helper

        private string GetGoogleMapImageUrl()
        {
            return string.Format("http://maps.googleapis.com/maps/api/staticmap?center={0}&zoom={1}&size={2}x{3}&sensor=false&maptype=roadmap&&markers=color:blue%7Clabel:Location%7C{0}",
                        GetValue(),
                        _mapZoomFactor,
                        _mapWidth,
                        _mapHeight);
        }

        #endregion
    }
}