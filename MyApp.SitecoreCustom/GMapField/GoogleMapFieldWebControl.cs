﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml.Xsl;

namespace MyApp.SitecoreCustom.GMapField
{
    public class GoogleMapFieldWebControl : Sitecore.Web.UI.WebControls.FieldControl
    {
        private System.Web.UI.HtmlControls.HtmlGenericControl _imgMap;

        private int _mapWidth = 600;
        private int _mapHeight = 150;
        private int _mapZoomFactor = 14;

        public GoogleMapFieldWebControl():
            base()
        {
            
        }

        protected override void CreateChildControls()
        {
            base.CreateChildControls();

            InitializeGoogleMap();
        }

        private void InitializeGoogleMap()
        {
            var item = GetItem();
            if (item != null)
            {
                //get field value in item
                var field = item.Fields[Field];

                if (field != null && !string.IsNullOrWhiteSpace(field.Value))
                {
                    //start div
                    var divStart = new System.Web.UI.HtmlControls.HtmlGenericControl("div");

                    _imgMap = new System.Web.UI.HtmlControls.HtmlGenericControl("img");
                    _imgMap.ID = string.Format("{0}_{1}",
                        ID,
                        "MapCanvas");

                    _imgMap.Attributes["src"] = GetGoogleMapImageUrl(field.Value);
                    
                    divStart.Controls.Add(_imgMap);
                    Controls.Add(divStart);
                }
            }
        }

        protected override void Render(HtmlTextWriter output)
        {
            if (string.IsNullOrEmpty(this.Field))
            {
                throw new InvalidOperationException("Field property is required. All field web controls require the field name to be set.");
            }

            RenderFieldResult renderFieldResult = new FieldRenderer
            {
                Item = this.GetItem(),
                FieldName = this.Field,
                Parameters = Parameters,
                DisableWebEditing = this.DisableWebEditing
            }.RenderField();
            output.Write(renderFieldResult.FirstPart);
            this.RenderChildren(output);
            output.Write(renderFieldResult.LastPart);
        }

        private string GetGoogleMapImageUrl(string longLat)
        {
            return string.Format("http://maps.googleapis.com/maps/api/staticmap?center={0}&zoom={1}&size={2}x{3}&sensor=false&maptype=roadmap&&markers=color:blue%7Clabel:Location%7C{0}",
                        longLat,
                        _mapZoomFactor,
                        _mapWidth,
                        _mapHeight);
        }
    }
}