﻿using System;
using Sitecore;
using Sitecore.Shell.Applications.WebEdit.Commands;
using Sitecore.Shell.Framework.Commands;
using Sitecore.Web;
using Sitecore.Web.UI.Sheer;

namespace MyApp.SitecoreCustom.GMapField.Command
{
    public class GoogleMapWebEditCommand : WebEditCommand
    {
        #region Overrides of Command

        public override void Execute(CommandContext context)
        {
            var formValue = WebUtil.GetFormValue("scPlainValue");

            //add current location to parameter
            context.Parameters.Add("value", formValue);

            Sitecore.Context.ClientPage.Start(this, "Run", context.Parameters);
        }

        #endregion

        protected void Run(ClientPipelineArgs args)
        {
            if (args.IsPostBack)
            {
                if (!args.HasResult) return;

                SheerResponse.SetAttribute("scHtmlValue", "value", args.Result);
                SheerResponse.SetAttribute("scPlainValue", "value", args.Result);
                SheerResponse.Eval("scSetHtmlValue('" + args.Parameters["controlid"] + "')");
            }
            else
            {
                var currentLocation = args.Parameters["value"];
                var url = UIUtil.GetUri("control:GmapLocationPickerDialog");

                if (!string.IsNullOrWhiteSpace(currentLocation))
                {
                    url = string.Format("{0}&value={1}",
                        url, currentLocation);
                }

                Sitecore.Context.ClientPage.ClientResponse.ShowModalDialog(url, true);
                args.WaitForPostBack();
            }
        }
    }
}