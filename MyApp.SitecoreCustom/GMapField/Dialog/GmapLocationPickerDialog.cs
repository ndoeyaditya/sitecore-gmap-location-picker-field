﻿using Sitecore.Web;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Web.UI.Pages;
using Sitecore.Web.UI.Sheer;

namespace MyApp.SitecoreCustom.GMapField.Dialog
{
    public class GmapLocationPickerDialog : DialogForm
    {
        #region Control

        public Edit TextBox_SelectedLocation;

        #endregion

        #region Control Event

        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            var value = WebUtil.GetQueryString("value");
            if (!string.IsNullOrWhiteSpace(value) && string.IsNullOrWhiteSpace(TextBox_SelectedLocation.Value))
            {
                TextBox_SelectedLocation.Value = value;
            }
        }

        protected override void OnOK(object sender, System.EventArgs args)
        {
            SheerResponse.SetDialogValue(TextBox_SelectedLocation.Value);
            base.OnOK(sender, args);
        }

        #endregion
    }
}